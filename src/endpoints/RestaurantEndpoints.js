export const restaurantSignUp =
  'https://hatyiyuei4.execute-api.us-east-1.amazonaws.com/prod/auth-service/restaurant/signup';

export const hiredCandidateEndpoint =
  'https://hatyiyuei4.execute-api.us-east-1.amazonaws.com/prod/restaurant-service/vacancy';

export const getRestaurantById =
  'https://hatyiyuei4.execute-api.us-east-1.amazonaws.com/prod/restaurant-service';

export const rechargeBalance =
  'https://hatyiyuei4.execute-api.us-east-1.amazonaws.com/prod/restaurant-service/recharge';

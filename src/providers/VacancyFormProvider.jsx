import React, { useContext, useState, createContext } from 'react';
import { UserAuthContext } from './AuthProvider';
import axios from 'axios';
import { createVacancy, updateVacancy, deleVacancyEndpoint } from '../endpoints/VacancyEndpoints';

export const VacancyFormContext = createContext({
  formSubmit: function () {},
  deleteVacancy: function () {},
  loading: false,
});

const VacancyFormProvider = ({ children }) => {
  const { token, updateTokenBalance } = useContext(UserAuthContext);
  const [loading, setLoading] = useState(false);
  const [title, setTitle] = useState('');
  const [message, setMessage] = useState('');
  const [open, setOpen] = useState(false);

  const formSubmit = async (values) => {
    const request = formatRequest(values);
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    const endpoint = createVacancy;
    try {
      setLoading(true);
      const { data } = await axios.post(endpoint, request, config);
      updateTokenBalance(data.token, data.balance);
      displayMessage(
        'Oferta creada',
        'Pudes observar los detalles de la oferta en la sección de ofertas',
        true
      );
    } catch (error) {
      displayMessage('Ha ocurrido un error', error.response.data.error, true);
    } finally {
      setLoading(false);
    }
  };

  const editVacancy = async (values) => {
    const { vacancyId } = values;
    delete values.vacancyId;
    const position = formatPosition(values.position);
    const requestValues = { ...values };
    delete requestValues.position;
    const request = {
      ...requestValues,
      position,
    };
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    const endpoint = `${updateVacancy}/${vacancyId}`;
    try {
      setLoading(true);
      await axios.put(endpoint, request, config);
      displayMessage('Oferta editada', 'Se ha editado con exito la oferta', true);
    } catch (error) {
      displayMessage('Ha ocurrido un error', error.response.data.error, true);
    } finally {
      setLoading(false);
    }
  };

  const deleteVacancy = async (vacancyId) => {
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    const endpoint = `${deleVacancyEndpoint}/${vacancyId}`;
    try {
      await axios.delete(endpoint, config);
      displayMessage('Oferta eliminada', 'Se ha eliminado con exito la oferta', true);
    } catch (error) {
      displayMessage('Lo sentimos', error.response.data.error, true);
    }
  };

  const formatPosition = (position) => {
    if (position === 'cocinero') return 1;
    if (position === 'mesero') return 2;
    if (position === 'lavaplatos') return 3;
    return 0;
  };

  const formatDate = (date, hour) => {
    return `${date} ${hour}`;
  };

  const formatRequest = (values) => {
    const position = formatPosition(values.position);
    const start_at = formatDate(values.start_at, values.start_hour);
    const end_at = formatDate(values.end_at, values.end_hour);
    return {
      description: values.description,
      start_at,
      end_at,
      offers_quantity: values.offers_quantity,
      payment_per_hour: values.payment_per_hour,
      country: values.country,
      state: values.state,
      city: values.city,
      address: values.address,
      position,
    };
  };

  const displayMessage = (message, title, open) => {
    setTitle(message);
    setMessage(title);
    setOpen(open);
  };

  const onClose = () => {
    setTitle('');
    setMessage('');
    setOpen(false);
  };

  return (
    <VacancyFormContext.Provider
      value={{ formSubmit, editVacancy, deleteVacancy, title, message, open, onClose, loading }}
    >
      {children}
    </VacancyFormContext.Provider>
  );
};

export default VacancyFormProvider;

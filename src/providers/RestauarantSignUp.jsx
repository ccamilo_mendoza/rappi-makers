import React, { useCallback, useState, createContext } from 'react';
import axios from 'axios';
import { restaurantSignUp } from '../endpoints/RestaurantEndpoints';

export const RestaurantSignUpContext = createContext({});

const RestaurantSignUpProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [open, setOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const formSubmit = useCallback(async (values) => {
    const request = { ...values };
    try {
      setLoading(true);
      await axios.post(restaurantSignUp, request);
      setOpen(true);
    } catch (error) {
      setOpen(true);
      setError(true);
      setErrorMessage(error.response.data.error);
    } finally {
      setLoading(false);
    }
  }, []);

  const onClose = () => {
    setOpen(false);
    setError(false);
    setErrorMessage('');
  };

  return (
    <RestaurantSignUpContext.Provider value={{ formSubmit, onClose, open, error, errorMessage, loading }}>
      {children}
    </RestaurantSignUpContext.Provider>
  );
};

export default RestaurantSignUpProvider;

import React, { useCallback, useState, createContext } from 'react';
import axios from 'axios';
import { userSignUp } from '../endpoints/UserEndpoints';

export const UserSignUpContext = createContext({});

const UserSignUpProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [open, setOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const formSubmit = useCallback(async (values) => {
    const positions = formatRequestPositions(values.positions);
    const requestValues = { ...values };
    delete requestValues.positions;
    const request = { dni_type_id: 1, ...requestValues, positions };
    try {
      setLoading(true);
      await axios.post(userSignUp, request);
      setOpen(true);
    } catch (error) {
      setOpen(true);
      setError(true);
      setErrorMessage(error.response.data.error);
    } finally {
      setLoading(false);
    }
  }, []);

  const onClose = () => {
    setOpen(false);
    setError(false);
    setErrorMessage('');
  };

  const formatRequestPositions = (positions) => {
    return positions.map((position) => {
      if (position === 'cocinero') return 1;
      if (position === 'mesero') return 2;
      if (position === 'lavaplatos') return 3;
      return 0;
    });
  };

  return (
    <UserSignUpContext.Provider value={{ formSubmit, onClose, open, error, errorMessage, loading }}>
      {children}
    </UserSignUpContext.Provider>
  );
};

export default UserSignUpProvider;

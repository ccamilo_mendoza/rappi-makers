export const inputProps = [
  {
    name: 'name',
    type: 'text',
    label: 'Nombre',
  },
  {
    name: 'nit',
    type: 'text',
    label: 'NIT',
  },
  {
    name: 'email',
    type: 'text',
    label: 'Email',
    placeholder: 'example@example.com',
  },
  {
    name: 'password',
    type: 'password',
    label: 'Contraseña',
  },
  {
    name: 'mobile',
    type: 'text',
    label: 'Número de celular',
  },

  {
    name: 'country',
    type: 'text',
    label: 'País de residencia',
  },
  {
    name: 'state',
    type: 'text',
    label: 'Departamento',
  },
  {
    name: 'city',
    type: 'text',
    label: 'Ciudad',
  },
  {
    name: 'address',
    type: 'text',
    label: 'Dirección',
  },
  {
    name: 'description',
    type: 'text',
    label: 'Descripción',
  },
];

/* eslint-disable no-useless-escape */
import { object, string } from 'yup';

const formConfig = {
  formValues: {
    identity_card: '',
    names: '',
    last_names: '',
    gender: 'M',
    birthday: '',
    email: '',
    mobile: '',
    country: '',
    state: '',
    city: '',
    address: '',
    positions: ['cocinero'],
    password: '',
  },
  formValidation: object({
    names: string().min(5).required('Este campo es obligatorio'),
    last_names: string().min(5).required('Este campo es obligatorio'),
    identity_card: string()
      .matches(new RegExp(/^[0-9]*$/), 'Este campo debe contener solo números')
      .required('Este campo es obligatorio')
      .min(5, 'Debe tener como mínimo 5 digitos')
      .max(10, 'Debe tener como máximo 10 digitos'),
    birthday: string()
      .matches(
        new RegExp(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/),
        'La fehca debe estar con el siguiente formato YYYY-MM-DD'
      )
      .required('Este campo es obligatorio'),
    email: string().email('Formato de email erroneo').required('Este campo es obligatorio'),
    mobile: string()
      .matches(new RegExp(/^[0-9]*$/), 'Este campo debe contener solo números')
      .max(10),
    country: string().required('Este campo es obligatorio'),
    state: string().required('Este campo es obligatorio'),
    city: string().required('Este campo es obligatorio'),
    address: string().required('Este campo es obligatorio'),
    password: string()
      .matches(
        new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/),
        'La contraseña debe contener al menos un número'
      )
      .min(6, 'La contraseña debe ser mínimo de 6 caracteres'),
  }),
};

export default formConfig;

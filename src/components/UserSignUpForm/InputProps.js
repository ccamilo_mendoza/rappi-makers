export const inputProps = [
  {
    name: 'names',
    type: 'text',
    label: 'Nombres',
  },
  {
    name: 'last_names',
    type: 'text',
    label: 'Apellidos',
  },
  {
    name: 'email',
    type: 'text',
    label: 'Email',
    placeholder: 'example@example.com',
  },
  {
    name: 'password',
    type: 'password',
    label: 'Contraseña',
  },
  {
    name: 'identity_card',
    type: 'text',
    label: 'Identificación',
  },
  {
    name: 'birthday',
    type: 'date',
    label: 'Fecha de nacimiento',
  },
  {
    name: 'mobile',
    type: 'text',
    label: 'Número de celular',
  },
  {
    name: 'country',
    type: 'text',
    label: 'País de residencia',
  },
  {
    name: 'state',
    type: 'text',
    label: 'Departamento',
  },
  {
    name: 'city',
    type: 'text',
    label: 'Ciudad',
  },
  {
    name: 'address',
    type: 'text',
    label: 'Dirección',
  },
];

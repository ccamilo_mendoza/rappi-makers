import React, { useState, useEffect, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import VacancyDetail from '../../components/VacancyDetails/VacancyDetail';
import Spinner from '../../components/Spinner/Spinner';
import { getVacancyById } from '../../endpoints/VacancyEndpoints';
import './VacancyDetail.css';

const VacancyDetailProvider = () => {
  const { id } = useParams();
  const [loading, setLoading] = useState(true);
  const [vacancy, setVacancy] = useState({});

  const fetchVacancy = useCallback(async () => {
    try {
      setLoading(true);
      const endpoint = `${getVacancyById}/${id}`;
      const { data } = await axios.get(endpoint);
      setVacancy(data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }, [id]);

  useEffect(() => {
    fetchVacancy();
  }, [fetchVacancy]);

  const formatPayment = (payment) => {
    const number = parseInt(payment, 10);
    return number.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  };
  const formatDate = (vacancyDate) => {
    const options = {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      hour12: 'false',
    };
    const date = new Date(vacancyDate);
    return date.toLocaleString('es-ES', options);
  };

  return (
    <div className="vacancy-detail-container">
      <>
        {loading ? (
          <Spinner />
        ) : (
          <VacancyDetail vacancy={vacancy} formatPayment={formatPayment} formatDate={formatDate} />
        )}
      </>
    </div>
  );
};

export default VacancyDetailProvider;
